package com.ecole.MySchoo.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ExamResponseDto {
    private double mark;
    private LocalDate examDate;
    private Long studentId;
    private Long courseId;
    private String examType;
}
