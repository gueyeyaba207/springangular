package com.ecole.MySchoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MySchooApplication {

	public static void main(String[] args) {
		SpringApplication.run(MySchooApplication.class, args);
	}

}
